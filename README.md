# Test for junior

Prueba desarrollador junior.

### Descripción

Pequeño CRUD de usuarios con las siguientes funcionalidades:

- Crear, leer, actualizar y borrar usuarios.
- Rutas y funciones protegidas para usuarios Admin.
- Coockie de RememberMe de 7 días de duración.

