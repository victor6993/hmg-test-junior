<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
// use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulario para crear un nuevo usuario o editar uno existente, contiene los campos necesarios para dar de alta a usuarios o editarlos.
 */
class NewUserForm extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder->add('username', TextType::class, ['attr' => ['placeholder' => 'Username'], 'required' => false]);
    $builder->add('password', PasswordType::class, ['attr' => ['placeholder' => 'Password'], 'required' => false]);
    $builder->add('email', EmailType::class, ['attr' => ['placeholder' => 'Email'], 'required' => false]);
    $builder->add('roles', ChoiceType::class, ['placeholder' => 'Choose a role', 'choices' => [
      'User' => 'ROLE_USER',
      'Admin' => 'ROLE_ADMIN'
    ], 'required' => false]);
    $builder->add('comments', TextareaType::class, ['attr' => ['placeholder' => 'Comments', 'maxLength' => 100, 'resize' => 'none'], 'required' => false]);

  }

  // public function configureOptions(OptionsResolver $resolver)
  // {
  //   $resolver->setDefaults(['data_class' => User::class]);
  // }
}