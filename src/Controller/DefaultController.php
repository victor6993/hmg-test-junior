<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\User;
use App\Form\NewUserForm;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Psr\Log\LoggerInterface;

/**
 * Controlador por defecto, en ella estableceremos las rutas del CRUD (crear, leer, modificar y borrar usuarios).
 * Para las rutas de seguridad como login o logout ver SecurityController.php
 */
class DefaultController extends AbstractController
{

  /**
   * @Route("/", name = "landing")
   * Ruta por defecto de la página, accesible para todo tipo de usuarios, incluido anónimo, en ella se muestra el listado de usuarios, los cuales se podrán modificar sólo si se tiene permisos de administrador.
   */
  public function landing(EntityManagerInterface $em, Request $request, LoggerInterface $logger)
  {
    try {
      $form = $this->createForm(NewUserForm::class);
      $form->handleRequest($request);
      
      $userDB = $em->getRepository(User::class);
      $users = $userDB->findAll();
      
      $userForms = [];
      for ($i = 0; $i < sizeof($users); $i++) {
        $userForms[$i] = $form->createView();
      }
    } catch(\Exception $e) {
      $logger->error("Error searching for users in userDB");
    }
      
    return $this->render("landing.html.twig", ['users' => $users, 'userForms' => $userForms]);
  }


  /**
   * @Route("/user/new", name = "new-user")
   * @IsGranted("ROLE_ADMIN")
   * Ruta para crear un nuevo usuario, sólo accesible para usuarios con rol de administrador.
   * Mostrará por defecto un formulario para crear un usuario y una vez rellenamos y enviamos los datos del usuario que queremos crear, se leen los datos obtenidos del formulario e introduce dicho contenido en un nuevo usuario en la base de datos, en caso de haberse creado con éxito se creará un registro informativo que indica que se ha creado un usuario y se redirigirá a la página principal(la lista de usuarios), en caso de error se creará un registro de error indicando el error en sí y se redirigirá de nuevo a la página para crear usuarios.
   */
  public function newUser(EntityManagerInterface $em, Request $request, UserPasswordEncoderInterface $passwordEncoder, LoggerInterface $logger)
  {

    $form = $this->createForm(NewUserForm::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

      try {

        $data = $form->getData();
        $newUser = new User();

        $encodedPassword = $passwordEncoder->encodePassword($newUser, $data['password']);
        $newUser->setUsername($data['username']);
        $newUser->setPassword($encodedPassword);
        $newUser->setEmail($data['email']);
        $newUser->setRoles([$data['roles']]);
        $newUser->setComments($data['comments']);

        $em->persist($newUser);
        $em->flush($newUser);

        $logger->info("Created a new user");

        return $this->redirectToRoute('landing');

      } catch (\Exception $e) {

        $logger->error("Error creating the user => $e");
        return $this->redirectToRoute('new-user');

      }
    }

    return $this->render('new-user.html.twig', ['NewUserForm' => $form->createView()]);
  }


  /**
   * @Route("/user/{id}/update", methods = {"POST"}, name = "update-user")
   * @IsGranted("ROLE_ADMIN")
   * Ruta para modificar usuarios, sólo accesible para usuarios con rol de administrador.
   * Intentamos modificar el usuario definido por el id de la ruta, se leen los datos obtenidos del formulario y confirma que cada campo tenga contenido para modificar dicho contenido en la base de datos, en caso de haberse modificado con éxito se creará un registro informativo que indica que se ha modificado dicho usuario, en caso de error se creará un registro de error indicando el error en sí y el usuario que se quería modificar.
   * En cualquier caso se redirige a la página principal(la lista de usuarios).
   */
  public function updateUser(EntityManagerInterface $em, Request $request, int $id, UserPasswordEncoderInterface $passwordEncoder, LoggerInterface $logger)
  {

    $form = $this->createForm(NewUserForm::class);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {

      try {
        $data = $form->getData();
        $userToBeUpdated = $em->getRepository(User::class)->find($id);

        if ($data['username'] !== null) {
          $userToBeUpdated->setUsername($data['username']);
        }
        if ($data['email'] !== null) {
          $userToBeUpdated->setEmail($data['email']);
        }
        if ($data['password'] !== null) {
          $encodedPassword = $passwordEncoder->encodePassword($userToBeUpdated, $data['password']);
          $userToBeUpdated->setPassword($encodedPassword);
        }
        if ($data['roles'] !== null) {
          $userToBeUpdated->setRoles([$data['roles']]);
        }
        if ($data['comments'] !== null) {
          $userToBeUpdated->setComments($data['comments']);
        }

        $em->persist($userToBeUpdated);
        $em->flush($userToBeUpdated);

        $logger->info("Updated the user with id $id");

      } catch (\Exception $e) {

        $logger->error("Error updating the user with id $id => $e");
      
      }
    }

    return $this->redirectToRoute("landing");
  }


  /**
   * @Route("/user/{id}/delete", name = "delete-user")
   * @IsGranted("ROLE_ADMIN")
   * Ruta para borrar usuarios, sólo accesible para usuarios con rol de administrador.
   * Intentamos borrar el usuario definido por el id de la ruta, en caso de haberse borrado con éxito se creará un registro informativo que indica que se ha borrado dicho usuario, en caso de error se creará un registro de error indicando el error en sí y el usuario que se quería borrar.
   * En cualquier caso se redirige a la página principal(la lista de usuarios).
   */
  public function deleteUser(User $user, EntityManagerInterface $em, int $id, LoggerInterface $logger)
  {
    try {
      $em->remove($user);
      $em->flush();

      $logger->info("Deleted the user with id $id");

    } catch(\Exception $e) {
      $logger->error("Error deleting the user with id $id => $e");
    }

    return $this->redirectToRoute("landing");
  }

}
